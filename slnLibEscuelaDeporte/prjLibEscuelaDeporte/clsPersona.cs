﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libPersona
{
    public abstract class clsPersona
    {
        #region Atributos
       protected  string strNombre;
       protected  string strApellido;
       protected  string strDocumento;
       protected  int intEdad;
       protected  int intTelefono;
       protected  string strUsuario;
       protected  string strContasena;

        #endregion

        #region Constructor
        public clsPersona()
        {
            this.strNombre = string.Empty;
            this.strApellido = string.Empty;
            this.strDocumento = string.Empty;
            this.intEdad = 0;
            this.intTelefono = -1;
            this.strUsuario = string.Empty;
            this.strContasena = string.Empty;
        }
        #endregion

        #region Propiedades
        public string Nombre { get => strNombre; set => strNombre = value; }
        public string Apellido { get => strApellido; set => strApellido = value; }
        public int Edad { get => intEdad; set => intEdad = value; }
        public string Documento { get => strDocumento; set => strDocumento = value; }
        public int Telefono { get => intTelefono; set => intTelefono = value; }
        public string Usuario { get => strUsuario; set => strUsuario = value; }
        public string Contasena { get => strContasena; set => strContasena = value; }
        #endregion

        #region Metodos Publicos
        public abstract bool registrar();
        public abstract bool validarSesion();
        public abstract bool borrar();
        public abstract bool actualizar();
        public abstract bool consultar();
        #endregion
    }
}
