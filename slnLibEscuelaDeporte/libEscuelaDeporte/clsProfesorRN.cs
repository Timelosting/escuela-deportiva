﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libConexionBd;
using libPersona;

namespace libEscuelaDeporte
{
    public class clsProfesorRN : clsPersona
    {
        #region Constantes
        private const int CERO = 0;
        private const string BORRAR = "borrar";
        private const string VALIDARSESION = "validarsesion";
        private const string REGISTRAR = "registrar";
        private const string ACTUALIZAR = "actualizar";
        #endregion

        #region Atributos
        private string strNombreApp;
        private string strError;
        private object vrUnico;
        private SqlParameter[] objDatosRegistro;
        private clsConexionBd objCnx;
        private DataSet dsDatos;
        #endregion

        #region Constructor
        public clsProfesorRN(string strNombreApp)
        {
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
        }

        #endregion

        #region Propiedades
        public string Error { get => strError; }
        public object VrUnico { get => vrUnico; set => vrUnico = value; }
        public DataSet DsDatos { get => dsDatos; }
        #endregion

        #region Metodos Privados 
        private bool Validar(string validarDirector)
        {
            if (strNombreApp == string.Empty)
            {
                strError = "Olvidó enviar el nombre de la aplicación";
                return false;
            }
            switch (validarDirector)
            {
                case VALIDARSESION:
                    if (strContasena == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (strUsuario == string.Empty)
                    {
                        strError = "Debe ingresar un usuario";
                        return false;
                    }
                    break;
                case REGISTRAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    if (strNombre == string.Empty)
                    {
                        strError = "Debe ingresar el nombre";
                        return false;
                    }
                    if (strApellido == string.Empty)
                    {
                        strError = "Debe ingresar el apellido";
                        return false;
                    }
                    if (strContasena == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (strUsuario == string.Empty)
                    {
                        strError = "Debe ingresar un usuario";
                        return false;
                    }
                    if (intEdad <= CERO)
                    {
                        strError = "La edad no puede ser menor o igual a 0";
                        return false;
                    }
                    break;
                case ACTUALIZAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    if (strNombre == string.Empty)
                    {
                        strError = "Debe ingresar el nombre";
                        return false;
                    }
                    if (strApellido == string.Empty)
                    {
                        strError = "Debe ingresar el apellido";
                        return false;
                    }
                    if (strContasena == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (intEdad <= CERO)
                    {
                        strError = "La edad no puede ser menor o igual a 0";
                        return false;
                    }
                    break;
                case BORRAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    break;
            }
            return true;

        }
        private bool agregarParametros(string metodoOrgien)
        {
            try
            {
                objDatosRegistro = new SqlParameter[1];

                switch (metodoOrgien.ToLower())
                {
                    case REGISTRAR:
                        if (!Validar(REGISTRAR))
                        {
                            return false;
                        }
                        objDatosRegistro = new SqlParameter[7];
                        objDatosRegistro[0] = new SqlParameter("@documento", strDocumento);
                        objDatosRegistro[1] = new SqlParameter("@nombre", strNombre);
                        objDatosRegistro[2] = new SqlParameter("@apellido", strApellido);
                        objDatosRegistro[3] = new SqlParameter("@edad", intEdad);
                        objDatosRegistro[4] = new SqlParameter("@telefono", intTelefono);
                        objDatosRegistro[5] = new SqlParameter("@usuario", strUsuario);
                        objDatosRegistro[6] = new SqlParameter("@contrasena", strContasena);
                        break;
                    case VALIDARSESION:
                        if (!Validar(VALIDARSESION))
                        {
                            return false;
                        }
                        objDatosRegistro = new SqlParameter[2];
                        objDatosRegistro[0] = new SqlParameter("@usuario", strUsuario);
                        objDatosRegistro[1] = new SqlParameter("@contrasena", strContasena);
                        break;
                    case BORRAR:
                        if (!Validar(BORRAR))
                        {
                            return false;
                        }
                        objDatosRegistro = new SqlParameter[1];
                        objDatosRegistro[0] = new SqlParameter("@documento", strDocumento);
                        break;
                    case ACTUALIZAR:
                        if (!Validar(ACTUALIZAR))
                        {
                            return false;
                        }
                        objDatosRegistro = new SqlParameter[6];
                        objDatosRegistro[0] = new SqlParameter("@documento", strDocumento);
                        objDatosRegistro[1] = new SqlParameter("@nombre", strNombre);
                        objDatosRegistro[2] = new SqlParameter("@apellido", strApellido);
                        objDatosRegistro[3] = new SqlParameter("@edad", intEdad);
                        objDatosRegistro[4] = new SqlParameter("@telefono", intTelefono);
                        objDatosRegistro[5] = new SqlParameter("@contrasena", strContasena);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Metodos Publicos
        public override bool registrar()
        {
            try
            {
                if (!agregarParametros(REGISTRAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_RegistrarProfesor";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override bool validarSesion()
        {
            try
            {
                if (!agregarParametros(VALIDARSESION))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_ValidaSesionProfesor";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.consultarValorUnico(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                vrUnico = objCnx.ValorUnico;
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override bool borrar()
        {
            try
            {
                if (!agregarParametros(BORRAR))
                {
                    return false;
                }

                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_eliminarProfesor";
                objCnx.ParametrosSQL = objDatosRegistro;

                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public override bool actualizar()
        {
            try
            {
                if (!agregarParametros(ACTUALIZAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_actualizarProfesor";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override bool consultar()
        {
            try
            {
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_ConsultarProfesor";
                if (!objCnx.llenarDataSet(false, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                dsDatos = objCnx.DataSetLleno;
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

    }
}
