﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libConexionBd;


namespace libEscuelaDeporte
{
    public class clsDeporteRN
    {
        #region Constantes
        private const int CERO = 0;
        private const string REGISTRAR = "registrar";
        private const string BORRAR = "borrar"; 
        #endregion

        #region Atributos
        private string strNombreDeporte;
        private string strNombreApp;
        private string strError;
        private int intIdTipoDeporte;
        private SqlParameter[] objDatosRegistro;
        private clsConexionBd objCnx;
        private DataSet dsDatos;
        #endregion

        #region Constructor
        public clsDeporteRN(string strNombreApp)
        {
            this.strNombreDeporte = string.Empty;
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
            this.intIdTipoDeporte = CERO;
        }

        #endregion

        #region Propiedades
        public string NombreDeporte { set => strNombreDeporte = value; }
        public string Error { get => strError; }
        public int IdTipoDeporte {  set => intIdTipoDeporte = value; }
        public DataSet DsDatos { get => dsDatos; }
        #endregion

        #region Metodos Privados
        private bool Validar(string Validar)
        {
            if (strNombreApp == string.Empty)
            {
                strError = "Debe enviar el nombre de la aplicacion";
                return false;
            }
            switch (Validar)
            {
                case REGISTRAR:
                    if (strNombreDeporte == string.Empty)
                    {
                        strError = "Debe ingresar un nombre de deporte";
                        return false;
                    }
                    if (intIdTipoDeporte <= CERO)
                    {
                        strError = "debe seleccionar un tipo de deporte valido";
                        return false;
                    }
                    break;
                case BORRAR:
                    if (strNombreDeporte == string.Empty)
                    {
                        strError = "Debe ingresar un nombre de deporte";
                        return false;
                    }
                    break;
            }
            return true;
        }

        private bool agregarParametros(string metodoOrigen)
        {
            try
            {
                if (!Validar(metodoOrigen.ToLower()))
                {
                    return false;
                }
                objDatosRegistro = new SqlParameter[1];

                switch (metodoOrigen.ToLower())
                {
                    case REGISTRAR:
                        objDatosRegistro = new SqlParameter[2];
                        objDatosRegistro[0] = new SqlParameter("@deporte", strNombreDeporte);
                        objDatosRegistro[1] = new SqlParameter("@tipoDeporte", intIdTipoDeporte);
                        break;
                    case BORRAR:
                        objDatosRegistro = new SqlParameter[1];
                        objDatosRegistro[0] = new SqlParameter("@deporte", strNombreDeporte);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public bool registrar()
        {
            try
            {
                if (!agregarParametros(REGISTRAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_RegistrarDeporte";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool borrar()
        {
            try
            {
                if (!agregarParametros(BORRAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_eliminarDeporte";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool consultar()
        {
            try
            {
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_ConsultarDeporte";
                if (!objCnx.llenarDataSet(false, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                dsDatos = objCnx.DataSetLleno;
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
        #endregion
    }
}
