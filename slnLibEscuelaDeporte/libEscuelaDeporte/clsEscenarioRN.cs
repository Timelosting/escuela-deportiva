﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libConexionBd;


namespace libEscuelaDeporte
{
    public class clsEscenarioRN
    {
        #region Constantes
        private const int CERO = 0;
        private const string REGISTRAR = "registrar";
        private const string BORRAR = "borrar"; 
        #endregion

        #region Atributos
        private int intIdEscenario;
        private string strNombreEscenario;
        private string strNombreApp;
        private string strError;
        private int intIdTipoDeporte;
        private SqlParameter[] objDatosRegistro;
        private clsConexionBd objCnx;
        private DataSet dsDatos;

        #endregion

        #region Constructor
        public clsEscenarioRN(string strNombreApp)
        {
            this.strNombreEscenario = string.Empty;
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
            this.intIdTipoDeporte = CERO;
            this.intIdEscenario = CERO;
        }

     

        #endregion

        #region Propiedades
        public string NombreEscenario {set => strNombreEscenario = value; }
        public string NombreApp { set => strNombreApp = value; }
        public string Error { get => strError; }
        public int IdTipoDeporte { set => intIdTipoDeporte = value; }
        public int IdEscenario { set => intIdEscenario = value; }
        public DataSet DsDatos { get => dsDatos; }
        #endregion

        #region Metodos Privados
        private bool Validar(string validarEscenario)
        {
            if (strNombreApp == string.Empty)
            {
                strError = "Olvidó enviar el nombre de la aplicación";
                return false;
            }
            switch (validarEscenario)
            {
                case REGISTRAR:
                    if (strNombreEscenario == string.Empty)
                    {
                        strError = "Debe ingresar un escenario";
                        return false;
                    }
                    break;
                case BORRAR:
                    if (intIdEscenario <= CERO)
                    {
                        strError = "el id de escenario no puede ser menor o igual que 0";
                        return false;
                    }
                    break;
            }
            return true;
        }
        private bool agregarParametros(string metodoOrigen)
        {
            try
            {
                if (!Validar(metodoOrigen.ToLower()))
                {
                    return false;
                }
                objDatosRegistro = new SqlParameter[1];

                switch (metodoOrigen.ToLower())
                {
                    case REGISTRAR:
                        objDatosRegistro = new SqlParameter[2];
                        objDatosRegistro[0] = new SqlParameter("@escenario", strNombreEscenario);
                        objDatosRegistro[1] = new SqlParameter("@tipoDeporte", intIdTipoDeporte);
                        break;
                    case BORRAR:
                        objDatosRegistro = new SqlParameter[1];
                        objDatosRegistro[0] = new SqlParameter("@idEscenario", intIdEscenario);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public bool registrar()
        {
            try
            {
                if (!agregarParametros(REGISTRAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_RegistrarEscenario";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool borrar()
        {
            try
            {
                if (!agregarParametros(BORRAR))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_eliminarEscenario";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.ejecutarSentencia(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public  bool consultar()
        {
            try
            {
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_ConsultarEscenario";
                if (!objCnx.llenarDataSet(false, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                dsDatos = objCnx.DataSetLleno;
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
