﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libLlenarCombos;
using libConexionBd;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace libEscuelaDeporte
{
    public class clsLlenarComboRN
    {
        #region Atributos
        private SqlParameter[] objDatosDrop;
        private string strTipoConsulta;
        private string strNombreApp;
        private string strDeporte;
        private int intIdHorario;
        private string strError;
        #endregion

        #region Constructor
        public clsLlenarComboRN( string strNombreApp)
        {
            this.strTipoConsulta = string.Empty;
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
        }
        #endregion

        #region Propiedades
        public string Error { get => strError;}
        public string Deporte {set => strDeporte = value; }
        public int IdHorario { set => intIdHorario = value; }
        #endregion

        #region Metodos Privados
        private bool Validar()
        {
            if (strNombreApp == string.Empty)
            {
                strError = "Olvidó enviar el nombre de la aplicación";
                return false;
            }
            return true;
        }
        private bool agregarParametros(string parametro)
        {
            try
            {
                if (!Validar())
                {
                    return false;
                }
                switch (parametro)
                {
                    case "tipoconsulta":
                        objDatosDrop = new SqlParameter[1];
                        objDatosDrop[0] = new SqlParameter("@TipoConsulta", strTipoConsulta);
                        break;
                    case "escenario":
                        objDatosDrop = new SqlParameter[1];
                        objDatosDrop[0] = new SqlParameter("@deporte", strDeporte);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public bool llenarDropDowns(DropDownList ddlGen)
        {
            try
            {
                string strCampoTexto = string.Empty;
                string strCampoId = string.Empty;

                switch (ddlGen.ID.ToLower())
                {
                    case "ddldeporte":
                        strTipoConsulta = "DEPORTE";
                        strCampoId = "deporte";
                        strCampoTexto = "deporte";
                        break;
                    case "ddlhorario":
                        strTipoConsulta = "HORARIO";
                        strCampoId = "idHorario";
                        strCampoTexto = "horario";
                        break;
                    case "ddltipodeporte":
                        strTipoConsulta = "TIPODEPORTE";
                        strCampoId = "idTipoDeporte";
                        strCampoTexto = "tipoDeporte";
                        break;
                    default:
                        strError = "Drop Down no válido";
                        return false;
                }
                if (!agregarParametros("tipoconsulta"))
                {
                    return false;
                }
                clsLlenarCombos objLlenar = new clsLlenarCombos(strNombreApp);
                objLlenar.SQL = "SP_ConsultarCombos";
                objLlenar.CampoID = strCampoId;
                objLlenar.CampoTexto = strCampoTexto;
                objLlenar.ParametrosSQL = objDatosDrop;
                if (!objLlenar.llenarComboWeb(ddlGen))
                {
                    strError = objLlenar.Error;
                    objLlenar = null;
                    return false;
                }
                objLlenar = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool llenarDropDownProfesor(DropDownList ddlGen)
        {
            try
            {
                string strCampoTexto = string.Empty;
                string strCampoId = string.Empty;

                strCampoId = "docProfesor";
                strCampoTexto = "nombreCompleto";
                clsLlenarCombos objLlenar = new clsLlenarCombos(strNombreApp);

                objLlenar.SQL = "SP_LlenarProfesor";
                objLlenar.CampoID = strCampoId;
                objLlenar.CampoTexto = strCampoTexto;
                if (!objLlenar.llenarComboWeb(ddlGen))
                {
                    strError = objLlenar.Error;
                    objLlenar = null;
                    return false;
                }
                objLlenar = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool llenarDropDownEscenario(DropDownList ddlGen)
        {
            try
            {
                string strCampoTexto = "";
                string strCampoId = "";

                strTipoConsulta = "DEPORTE";
                strCampoId = "idEscenario";
                strCampoTexto = "escenario";
                if (!agregarParametros("escenario"))
                {
                    return false;
                }
                clsLlenarCombos objLlenar = new clsLlenarCombos(strNombreApp);

                objLlenar.SQL = "SP_LlenarEscenario";
                objLlenar.CampoID = strCampoId;
                objLlenar.CampoTexto = strCampoTexto;
                objLlenar.ParametrosSQL = objDatosDrop;
                if (!objLlenar.llenarComboWeb(ddlGen))
                {
                    strError = objLlenar.Error;
                    objLlenar = null;
                    return false;
                }
                objLlenar = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
  #endregion
    }
}
