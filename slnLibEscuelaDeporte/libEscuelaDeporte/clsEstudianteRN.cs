﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libConexionBd;
using libPersona;
namespace libEscuelaDeporte
{
    public class clsEstudianteRN : clsPersona
    {
        #region Atributos
        private string strNombreApp;
        private string strError;
        private object vrUnico;
        private SqlParameter[] objDatosRegistro;
        private clsConexionBd objCnx;
        private DataSet dsDatos;
        #endregion

        #region Constructor
        public clsEstudianteRN(string strNombreApp)
        {
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
        }

        #endregion

        #region Propiedades
        public string Error { get => strError; }
        public object VrUnico { get => vrUnico; }
        public DataSet DsDatos { get => dsDatos; }
        #endregion

        #region Metodos Privados 
        private bool Validar()
        {
            if (strNombreApp == string.Empty)
            {
                strError = "Olvidó enviar el nombre de la aplicación";
                return false;
            }

            if (strContasena == string.Empty)
            {
                strError = "Debe ingresar una contraseña";
                return false;
            }
            if (strUsuario == string.Empty)
            {
                strError = "Debe ingresar un usuario";
                return false;
            }
            return true;
        }

        private bool agregarParametros(string metodoOrgien)
        {
            try
            {
                if (!Validar())
                {
                    return false;
                }
                objDatosRegistro = new SqlParameter[2];
                objDatosRegistro[0] = new SqlParameter("@usuario", strUsuario);
                objDatosRegistro[1] = new SqlParameter("@contrasena", strContasena);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public override bool registrar()
        {
            return true;
        }
        public override bool validarSesion()
        {
            try
            {
                if (!agregarParametros("validarsesion"))
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_ValidaSesionEstudiante";
                objCnx.ParametrosSQL = objDatosRegistro;
                if (!objCnx.consultarValorUnico(true, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                vrUnico = objCnx.ValorUnico;
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public override bool borrar()
        {
            throw new NotImplementedException();
        }
        public override bool actualizar()
        {
            throw new NotImplementedException();
        }
        public override bool consultar()
        {
            try
            {
                objCnx = new clsConexionBd(strNombreApp);
                objCnx.SQL = "SP_ConsultarEstudiante";
                if (!objCnx.llenarDataSet(false, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                dsDatos = objCnx.DataSetLleno;
                objCnx.cerrarCnx();
                objCnx = null;
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
    }
}
