﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;
using System.Data.SqlClient;

namespace prjEscuelaDeporte
{
    public partial class frmVerEscenario : System.Web.UI.Page
    {
        #region Variables globales
        private static string strNombreApp;
        #endregion

        #region MetodosPrivados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h5";
            }
        }
        private void MostrarDatos()
        {
            try
            {
                clsEscenarioOP escenario = new clsEscenarioOP(strNombreApp);
                if (!escenario.consultarOP(gvEscenario))
                {
                    mostrarMsj(escenario.Error, true);
                    escenario = null;
                    return;
                }
                escenario = null;
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        private void eliminarDatos(GridViewDeleteEventArgs e)
        {
            try
            {
                clsEscenarioOP escenario = new clsEscenarioOP(strNombreApp);
                escenario.idEscenario = (int)gvEscenario.DataKeys[e.RowIndex].Values[0];
                if (!escenario.borrarOP())
                {
                    mostrarMsj(escenario.Error, true);
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj("Ups.. Algo ha salido \n NOTA: ten en cuenta que no se puede eliminar un escenario asociado a un grupo \n Detalles:"+ex.Message, true);
            }
        }
        #endregion

        #region Evento
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                MostrarDatos();
            }
        }
        protected void rowDeletingEvent(object sender, GridViewDeleteEventArgs e)
        {
            eliminarDatos(e);
            if (IsPostBack)
            {
                Server.Transfer("frmVerEscenario.aspx");
            }
        }
        #endregion
    }
}