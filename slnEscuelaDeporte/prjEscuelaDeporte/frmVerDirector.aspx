﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmVerDirector.aspx.cs" Inherits="prjEscuelaDeporte.frmVerDirector" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/registroDirector.css" rel="stylesheet" />
    <link href="img/fond.jpg" rel="icon" />
    <title>Directores</title>
</head>
<body>
    <form id="frmVerDirector" runat="server">
        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
            <a href="frmDirector.aspx" class="navbar-brand">Escuela de deportes</a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle active" data-toggle="dropdown">Directores</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmRegistrarDirector.aspx">Registrar Director</a>
                        <a class="dropdown-item active" href="frmVerDirector.aspx">Ver Director</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Padres</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmPadre.aspx">Registrar Padre</a>
                        <a class="dropdown-item" href="frmVerPadre.aspx">Ver Padre</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Docentes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmDocente.aspx">Registrar Docente</a>
                        <a class="dropdown-item" href="frmVerDocente.aspx">Ver Docente</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Estudiantes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmEstudiante.aspx">Ver Estudiantes</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Deportes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmDeporte.aspx">Crear Deporte</a>
                        <a class="dropdown-item" href="frmVerDeporte.aspx">Ver Deporte</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Escenarios Deportivos</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmEscenario.aspx">Registrar Escenario</a>
                        <a class="dropdown-item" href="frmVerEscenario.aspx">Ver Escenarios</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Grupos</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmGrupos.aspx">Crear Grupo</a>
                        <a class="dropdown-item" href="frmActualizarGrupo.aspx">Actualizar Grupo</a>
                        <a class="dropdown-item" href="frmVerGrupo.aspx">Ver Grupos</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Informes</a>
                </li>
                <li class="nav-item" style="margin-left: 9em;">
                    <a href="frmLogin.aspx" class="nav-link">Salir</a>
                </li>
            </ul>
        </nav>
        <div class="container">
            <asp:Panel runat="server" ID="pnlMensaje" Visible="false">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <asp:Label ID="Label1" runat="server" CssClass="text-center"></asp:Label>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </asp:Panel>
            <asp:GridView ID="gvDirector" runat="server" CssClass="table table-responsive table-bordered bg-dark table-striped"
                OnRowCancelingEdit="rowCancelEditEvent" 
                OnRowDeleting="rowDeletingEvent" 
                OnRowEditing="rowEditingEvent" 
                OnRowUpdating="rowUpdatingEvent" AutoGenerateColumns="false"
                DataKeyNames="DOCUMENTO">
                
                <Columns>
                    <asp:TemplateField HeaderText="Documento">
                        <ItemTemplate>
                            <asp:Label ID="lblDocumento" runat="server" Text='<%# Bind("DOCUMENTO")%>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDocumento" CssClass="txtD" runat="server" Text='<%# Bind("DOCUMENTO")%>' ReadOnly="true"/>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre">
                        <ItemTemplate>
                            <asp:Label ID="lblNombre" runat="server" Text='<%# Bind("NOMBRE")%>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNombre" CssClass="txtN" runat="server" Text='<%# Bind("NOMBRE")%>'/>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Apellido">
                        <ItemTemplate>
                            <asp:Label ID="lblApellido" runat="server" Text='<%# Bind("APELLIDO")%>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAPellido" CssClass="txtN" runat="server" Text='<%# Bind("APELLIDO")%>'/>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edad">
                        <ItemTemplate>
                            <asp:Label ID="lblEdad" runat="server" Text='<%# Bind("EDAD")%>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEdad" TextMode="Number" CssClass="txtE" runat="server" Text='<%# Bind("EDAD")%>'/>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Telefono">
                        <ItemTemplate>
                            <asp:Label ID="lblTelefono" runat="server" Text='<%# Bind("TELEFONO")%>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtTelefono" TextMode="Number" CssClass="txtT" runat="server" Text='<%# Bind("TELEFONO")%>'/>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Usuario">
                        <ItemTemplate>
                            <asp:Label ID="lblUsuario" runat="server" Text='<%# Bind("USUARIO")%>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUsuario" CssClass="txtU" runat="server" Text='<%# Bind("USUARIO")%>' ReadOnly="true"/>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" />   
                    
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblMensaje" runat="server" CssClass="text-center" Visible="false"></asp:Label>
        </div>
    </form>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
