﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmVerGrupo.aspx.cs" Inherits="prjEscuelaDeporte.frmVerGrupo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="img/fond.jpg" rel="icon" />
    <link href="css/grupo.css" rel="stylesheet" />
    <title>Grupos</title>
</head>
<body>
    <form id="frmVerGrupo" runat="server">
        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
            <a href="frmDirector.aspx" class="navbar-brand">Escuela de deportes</a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Directores</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmRegistrarDirector.aspx">Registrar Director</a>
                        <a class="dropdown-item" href="frmVerDirector.aspx">Ver Director</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Padres</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmPadre.aspx">Registrar Padre</a>
                        <a class="dropdown-item" href="frmVerPadre.aspx">Ver Padre</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Docentes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmDocente.aspx">Registrar Docente</a>
                        <a class="dropdown-item" href="frmVerDocente.aspx">Ver Docente</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Estudiantes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmEstudiante.aspx">Ver Estudiantes</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Deportes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmDeporte.aspx">Crear Deporte</a>
                        <a class="dropdown-item" href="frmVerDeporte.aspx">Ver Deporte</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Escenarios Deportivos</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmEscenario.aspx">Registrar Escenario</a>
                        <a class="dropdown-item" href="frmVerEscenario.aspx">Ver Escenarios</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle active" data-toggle="dropdown">Grupos</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmGrupos.aspx">Crear Grupo</a>
                        <a class="dropdown-item" href="frmActualizarGrupo.aspx">Actualizar Grupo</a>
                        <a class="dropdown-item active" href="frmVerGrupo.aspx">Ver Grupos</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Informes</a>
                </li>
                <li class="nav-item" style="margin-left: 9em;">
                    <a href="frmLogin.aspx" class="nav-link">Salir</a>
                </li>
            </ul>
        </nav>
        <div class="container">
            <asp:GridView ID="gvGrupo" runat="server" CssClass="table table-responsive table-bordered bg-dark table-striped"
                 OnRowDeleting="gvGrupo_RowDeleting"
                AutoGenerateColumns="false"
                DataKeyNames="ID_GRUPO">
                
                <Columns>
                    <asp:TemplateField HeaderText="ID">
                        <ItemTemplate>
                            <asp:Label ID="lblIdGrupo" runat="server" Text='<%# Bind("ID_GRUPO")%>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DEPORTE">
                        <ItemTemplate>
                            <asp:Label ID="lblDeporte" runat="server" Text='<%# Bind("DEPORTE")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HORARIO">
                        <ItemTemplate>
                            <asp:Label ID="lblHorario" runat="server" Text='<%# Bind("HORARIO")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ESCENARIO">
                        <ItemTemplate>
                            <asp:Label ID="lblEscenario" runat="server" Text='<%# Bind("ESCENARIO")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PROFESOR">
                        <ItemTemplate>
                            <asp:Label ID="lblProfesor" runat="server" Text='<%# Bind("PROFESOR")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:CommandField ButtonType="Link" ShowDeleteButton="true" />   
                    
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblMensaje" runat="server" CssClass="text-center" Visible="false"></asp:Label>
        </div>
    </form>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
