﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;

namespace prjEscuelaDeporte
{
    public partial class frmEscenario : System.Web.UI.Page
    {
        #region Variables globales
        private static string strNombreApp;
        #endregion

        #region Metodos Privados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h5";
            }
        }
        private bool validar()
        {
            if (this.txtNombre.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar el nombre del escenario", true);
                return false;
            }
            if (this.ddltipodeporte.SelectedIndex == 0)
            {
                mostrarMsj("Debe seleccionar el tipo del deporte", true);
                return false;
            }
            return true;
        }
        private void mostrarPanel()
        {
            this.pnlMensaje.Visible = true;
        }
        private void Limpiar()
        {
            this.txtNombre.Text = string.Empty;
            this.ddltipodeporte.SelectedIndex = 0;
        }
        private void registrar()
        {
            try
            {
                if (!validar())
                {
                    return;
                }
                clsEscenarioOP escenario = new clsEscenarioOP(strNombreApp);
                escenario.NombreEscenario = this.txtNombre.Text.Trim();
                escenario.IdTipoDeporte = int.Parse(this.ddltipodeporte.SelectedValue.Trim());
                if (!escenario.registrarOP())
                {
                    mostrarMsj(escenario.Error, true);
                    escenario = null;
                    return;
                }
                mostrarMsj("Registro Exitoso", false);
                mostrarPanel();
                escenario = null;
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        private void llenarDropDown(DropDownList ddlGen)
        {
            try
            {
                clsLlenarComboOP llenar = new clsLlenarComboOP(strNombreApp);
                llenar.DdlGen = ddlGen;
                if (!llenar.llenarDrop())
                {
                    mostrarMsj(llenar.Error, true);
                    llenar = null;
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                llenarDropDown(ddltipodeporte);
            }
        }
        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            registrar();
            Limpiar();
        }
        #endregion

    }
}