﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;
using System.Data.SqlClient;

namespace prjEscuelaDeporte
{
    public partial class frmVerDirector : System.Web.UI.Page
    {
        #region Variables globales
        private static string strNombreApp;
        #endregion

        #region MetodosPrivados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h5";
            }
        }
        private void MostrarDatos()
        {
            try
            {
                clsDirectorOP director = new clsDirectorOP(strNombreApp);
                if (!director.consultarOP(gvDirector))
                {
                    mostrarMsj(director.Error, true);
                    director = null;
                    return;
                }
                director = null;
            }
            catch (Exception ex)
            {

                mostrarMsj(ex.Message, true);
            }
        }
        private void eliminarDatos(GridViewDeleteEventArgs e)
        {
            try
            {
                clsDirectorOP director = new clsDirectorOP(strNombreApp);
                director.Documento = gvDirector.DataKeys[e.RowIndex].Values[0].ToString();
                if (!director.borrarOP())
                {
                    mostrarMsj(director.Error, true);
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        private void actualizarDatos(GridViewUpdateEventArgs e)
        {
            try
            {
                GridViewRow fila = gvDirector.Rows[e.RowIndex];
                clsDirectorOP director = new clsDirectorOP(strNombreApp);
                director.Documento = gvDirector.DataKeys[e.RowIndex].Values[0].ToString();
                director.Nombre = (fila.FindControl("txtNombre") as TextBox).Text;
                director.Apellido = (fila.FindControl("txtApellido") as TextBox).Text;
                director.Edad = int.Parse((fila.FindControl("txtEdad") as TextBox).Text);
                director.Telefono = int.Parse((fila.FindControl("txtTelefono") as TextBox).Text);
                if (!director.actualizarOP())
                {
                    mostrarMsj(director.Error, true);
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        #endregion

        #region Evento
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                MostrarDatos();
            }
        }
        protected void rowCancelEditEvent(object sender, GridViewCancelEditEventArgs e)
        {
            if (IsPostBack)
            {
                Server.Transfer("frmVerDirector.aspx");
            }
        }
        protected void rowDeletingEvent(object sender, GridViewDeleteEventArgs e)
        {
            eliminarDatos(e);
            if (IsPostBack)
            {
                Server.Transfer("frmVerDirector.aspx");
            }
        }
        protected void rowEditingEvent(object sender, GridViewEditEventArgs e)
        {
            gvDirector.EditIndex = e.NewEditIndex;
        }
        protected void rowUpdatingEvent(object sender, GridViewUpdateEventArgs e)
        {
            actualizarDatos(e);
            if (IsPostBack)
            {
                Server.Transfer("frmVerDirector.aspx");
            }
        }
        #endregion
    }
}