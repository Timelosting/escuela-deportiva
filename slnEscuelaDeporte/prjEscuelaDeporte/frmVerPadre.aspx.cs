﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;
using System.Data.SqlClient;

namespace prjEscuelaDeporte
{
    public partial class frmVerPadre : System.Web.UI.Page
    {
        #region Variables globales
        private static string strNombreApp;
        #endregion

        #region MetodosPrivados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h5";
            }
        }
        private void MostrarDatos()
        {
            try
            {
                clsPadreOP padre = new clsPadreOP(strNombreApp);
                if (!padre.consultarOP(gvPadre))
                {
                    mostrarMsj(padre.Error, true);
                    padre = null;
                    return;
                }
                padre = null;
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        private void eliminarDatos(GridViewDeleteEventArgs e)
        {
            try
            {
                clsPadreOP padre = new clsPadreOP(strNombreApp);
                padre.Documento = gvPadre.DataKeys[e.RowIndex].Values[0].ToString();
                if (!padre.borrarOP())
                {
                    mostrarMsj(padre.Error, true);
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        private void actualizarDatos(GridViewUpdateEventArgs e)
        {
            try
            {
                GridViewRow fila = gvPadre.Rows[e.RowIndex];
                clsPadreOP padre = new clsPadreOP(strNombreApp);
                padre.Documento = gvPadre.DataKeys[e.RowIndex].Values[0].ToString();

                padre.Nombre = (fila.FindControl("txtNombre") as TextBox).Text;
                padre.Apellido = (fila.FindControl("txtApellido") as TextBox).Text;
                padre.Edad = int.Parse((fila.FindControl("txtEdad") as TextBox).Text);
                padre.Telefono = int.Parse((fila.FindControl("txtTelefono") as TextBox).Text);
                padre.Contasena = (fila.FindControl("txtContrasena") as TextBox).Text;
                if (!padre.actualizarOP())
                {
                    mostrarMsj(padre.Error,true);
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        #endregion

        #region Evento
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                MostrarDatos();
            }
        }
        protected void rowCancelEditEvent(object sender, GridViewCancelEditEventArgs e)
        {
            if (IsPostBack)
            {
                Server.Transfer("frmVerPadre.aspx");
            }
        }
        protected void rowDeletingEvent(object sender, GridViewDeleteEventArgs e)
        {
            eliminarDatos(e);
            if (IsPostBack)
            {
                Server.Transfer("frmVerPadre.aspx");
            }
        }
        protected void rowEditingEvent(object sender, GridViewEditEventArgs e)
        {
            gvPadre.EditIndex = e.NewEditIndex;
        }
        protected void rowUpdatingEvent(object sender, GridViewUpdateEventArgs e)
        {
            actualizarDatos(e);
            if (IsPostBack)
            {
                Server.Transfer("frmVerPadre.aspx");
            }
        }
        #endregion
    }
}