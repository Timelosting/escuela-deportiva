﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmPerfilEstudiante.aspx.cs" Inherits="prjEscuelaDeporte.frmPerfilEstudiante" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="img/fond.jpg" rel="icon" />
    <title>Perfil Estudiante</title>
</head>
<body style="background-image:url(img/fondoEstudiante.jpg); background-size:cover;">
    <form id="frmPerfilEstudiante" runat="server">
        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
            <a href="frmPerfilEstudiante.aspx" class="navbar-brand">Perfil Estudiante</a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="#" class="nav-link">Horario De Clases</a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link">Matricularme</a>
                </li>
                <li class="nav-item" style="margin-left:30em;">
                    <a href="frmLogin.aspx" class="nav-link">Salir</a>
                </li>
            </ul>
        </nav>
    </form>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
