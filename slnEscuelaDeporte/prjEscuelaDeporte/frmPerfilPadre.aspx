﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmPerfilPadre.aspx.cs" Inherits="prjEscuelaDeporte.frmPadrePrincipal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="img/fond.jpg" rel="icon" />
    <title>Perfil Padre</title>
</head>
<body style="background-image:url(img/fondoPadre.jpg); background-size:cover;">
    <form id="frmPerfilPadre" runat="server">
        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
            <a href="frmPerfilPadre.aspx" class="navbar-brand">Perfil Padre</a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="#" class="nav-link">Registrar Estudiante</a>
                </li>
                <li class="nav-item" style="margin-left:1em;">
                    <a href="frmLogin.aspx" class="nav-link">Salir</a>
                </li>
            </ul>
        </nav>
    </form>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
