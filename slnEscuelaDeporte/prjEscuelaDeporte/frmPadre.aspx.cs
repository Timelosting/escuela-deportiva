﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;

namespace prjEscuelaDeporte
{
    public partial class frmPadre : System.Web.UI.Page
    {
        #region Variables globales
        private static string strNombreApp;
        #endregion

        #region Metodos Privados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h5";
            }
        }
        private bool validar()
        {
            if (this.txtDocumento.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar el documento de identidad", true);
                return false;
            }
            if (this.txtApellido.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar el apellido", true);
                return false;
            }
            if (this.txtEdad.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar la edad", true);
                return false;
            }
            if (this.txtNombre.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar el nombre", true);
                return false;
            }
            if (this.txtContraseña.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar la contraseña", true);
                return false;
            }
            if (this.txtTelefono.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar el telefono", true);
                return false;
            }
            if (this.txtUsuario.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar el usuario", true);
                return false;
            }
            return true;
        }
        private void mostrarPanel()
        {
            this.pnlMensaje.Visible = true;
        }
        private void Limpiar()
        {
            this.txtDocumento.Text = string.Empty;
            this.txtNombre.Text = string.Empty;
            this.txtApellido.Text = string.Empty;
            this.txtEdad.Text = string.Empty;
            this.txtTelefono.Text = string.Empty;
            this.txtUsuario.Text = string.Empty;
            this.txtContraseña.Text = string.Empty;
        }
        private void registrar()
        {
            try
            {
                if (!validar())
                {
                    return;
                }
                clsPadreOP padre = new clsPadreOP(strNombreApp);
                padre.Documento = this.txtDocumento.Text.Trim();
                padre.Nombre = this.txtNombre.Text.Trim();
                padre.Apellido = this.txtApellido.Text.Trim();
                padre.Edad = int.Parse(this.txtEdad.Text.Trim());
                padre.Telefono = int.Parse(this.txtTelefono.Text.Trim());
                padre.Usuario = this.txtUsuario.Text.Trim();
                padre.Contasena = this.txtContraseña.Text.Trim();

                if (!padre.registrarOP())
                {
                    mostrarMsj(padre.Error, true);
                    padre = null;
                    return;
                }
                mostrarMsj("Registro Exitoso", false);
                mostrarPanel();
                padre = null;
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
            }
        }
        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            registrar();
            Limpiar();
        }
        #endregion

    }
}