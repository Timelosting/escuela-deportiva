﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmGrupos.aspx.cs" Inherits="prjEscuelaDeporte.frmGrupos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/grupo.css" rel="stylesheet" />
    <link href="img/fond.jpg" rel="icon" />
    <title>Registro de grupos</title>
</head>
<body>
    <form id="frmPadre" runat="server">
        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
            <a href="frmDirector.aspx" class="navbar-brand">Escuela de deportes</a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Directores</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmRegistrarDirector.aspx">Registrar Director</a>
                        <a class="dropdown-item" href="frmVerDirector.aspx">Ver Director</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Padres</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmPadre.aspx">Registrar Padre</a>
                        <a class="dropdown-item" href="frmVerPadre.aspx">Ver Padre</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Docentes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmDocente.aspx">Registrar Docente</a>
                        <a class="dropdown-item" href="frmVerDocente.aspx">Ver Docente</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Estudiantes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmEstudiante.aspx">Ver Estudiantes</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Deportes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmDeporte.aspx">Crear Deporte</a>
                        <a class="dropdown-item" href="frmVerDeporte.aspx">Ver Deporte</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Escenarios Deportivos</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmEscenario.aspx">Registrar Escenario</a>
                        <a class="dropdown-item" href="frmVerEscenario.aspx">Ver Escenarios</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle active" data-toggle="dropdown">Grupos</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item active" href="frmGrupos.aspx">Crear Grupo</a>
                        <a class="dropdown-item" href="frmActualizarGrupo.aspx">Actualizar Grupo</a>
                        <a class="dropdown-item" href="frmVerGrupo.aspx">Ver Grupos</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Informes</a>
                </li>
                <li class="nav-item" style="margin-left:9em;">
                    <a href="frmLogin.aspx" class="nav-link">Salir</a>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="card">
                <div class="card-header text-center">
                    <h5>Registrar Grupo</h5>
                </div>
                <div class="card-body">
                    <asp:Panel runat="server" ID="pnlMensaje" Visible="false">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <asp:Label ID="lblMensaje" runat="server" CssClass="text-center"></asp:Label>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </asp:Panel>
                    <asp:Label Text="Deporte" CssClass="form-group" runat="server" />
                    <asp:DropDownList runat="server" AutoPostBack="true" ID="ddldeporte" CssClass="form-control" OnSelectedIndexChanged="ddldeporte_SelectedIndexChanged"></asp:DropDownList>
                    <br />
                    <asp:Label Text="Escenario" CssClass="form-group" runat="server" />
                    <asp:DropDownList runat="server" ID="ddlescenario" CssClass="form-control"></asp:DropDownList>
                    <br />
                    <asp:Label Text="Horario" CssClass="form-group" runat="server" />
                    <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlhorario" CssClass="form-control"></asp:DropDownList>
                    <br />
                    <asp:Label Text="Profesor" CssClass="form-group" runat="server" />
                    <asp:DropDownList runat="server" ID="ddlprofesor" CssClass="form-control"></asp:DropDownList>
                    <br />
                    <asp:Button Text="Registrar" ID="btnRegistrar" runat="server" cssClass="btn btn-block" OnClick="btnRegistrar_Click"/>
                </div>
            </div>
        </div>
    </form>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
