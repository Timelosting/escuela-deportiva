﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;
using System.Data.SqlClient;

namespace prjEscuelaDeporte
{
    public partial class frmVerDeporte : System.Web.UI.Page
    {
        #region Variables globales
        private static string strNombreApp;
        #endregion

        #region MetodosPrivados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h5";
            }
        }
        private void MostrarDatos()
        {
            try
            {
                clsDeporteOP deporte = new clsDeporteOP(strNombreApp);
                if (!deporte.consultarOP(gvDeporte))
                {
                    mostrarMsj(deporte.Error, true);
                    deporte = null;
                    return;
                }
                deporte = null;
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        private void eliminarDatos(GridViewDeleteEventArgs e)
        {
            try
            {
                clsDeporteOP deporte = new clsDeporteOP(strNombreApp);
                deporte.NombreDeporte = gvDeporte.DataKeys[e.RowIndex].Values[0].ToString();
                if(!deporte.borrarOP()){
                    mostrarMsj(deporte.Error, true);
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                MostrarDatos();
            }
        }
        protected void rowDeletingEvent(object sender, GridViewDeleteEventArgs e)
        {
            eliminarDatos(e);
            if (IsPostBack)
            {
                Server.Transfer("frmVerDeporte.aspx");
            }
        }
        #endregion
    }
}