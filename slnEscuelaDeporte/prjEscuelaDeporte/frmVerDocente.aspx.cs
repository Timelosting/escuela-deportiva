﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;
using System.Data.SqlClient;

namespace prjEscuelaDeporte
{
    public partial class frmVerDocente : System.Web.UI.Page
    {
        #region Variables globales
        private static string strNombreApp;
        #endregion

        #region MetodosPrivados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h5";
            }
        }
        private void MostrarDatos()
        {
            try
            {
                clsProfesorOP profesor = new clsProfesorOP(strNombreApp);
                if (!profesor.consultarOP(gvDocente))
                {
                    mostrarMsj(profesor.Error, true);
                    profesor = null;
                    return;
                }
                profesor = null;
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        private void eliminarDatos(GridViewDeleteEventArgs e)
        {
            try
            {
                clsProfesorOP profesor = new clsProfesorOP(strNombreApp);
                profesor.Documento = gvDocente.DataKeys[e.RowIndex].Values[0].ToString();
                if (!profesor.borrarOP())
                {
                    mostrarMsj(profesor.Error, true);
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message,true);
            }
        }
        private void actualizarDatos(GridViewUpdateEventArgs e)
        {
            try
            {
                GridViewRow fila = gvDocente.Rows[e.RowIndex];
                clsProfesorOP profesor = new clsProfesorOP(strNombreApp);
                profesor.Documento = gvDocente.DataKeys[e.RowIndex].Values[0].ToString();

                profesor.Nombre = (fila.FindControl("txtNombre") as TextBox).Text;
                profesor.Apellido = (fila.FindControl("txtApellido") as TextBox).Text;
                profesor.Edad = int.Parse((fila.FindControl("txtEdad") as TextBox).Text);
                profesor.Telefono = int.Parse((fila.FindControl("txtTelefono") as TextBox).Text);
                profesor.Contasena = (fila.FindControl("txtContrasena") as TextBox).Text;
                if (!profesor.actualizarOP())
                {
                    mostrarMsj(profesor.Error, true);
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message,true);
            }
        }
        #endregion

        #region Evento
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                MostrarDatos();
            }
        }
        protected void rowCancelEditEvent(object sender, GridViewCancelEditEventArgs e)
        {
            if (IsPostBack)
            {
                Server.Transfer("frmVerDocente.aspx");
            }
        }
        protected void rowDeletingEvent(object sender, GridViewDeleteEventArgs e)
        {
            eliminarDatos(e);
            if (IsPostBack)
            {
                Server.Transfer("frmVerDocente.aspx");
            }
        }
        protected void rowEditingEvent(object sender, GridViewEditEventArgs e)
        {
            gvDocente.EditIndex = e.NewEditIndex;

        }
        protected void rowUpdatingEvent(object sender, GridViewUpdateEventArgs e)
        {
            actualizarDatos(e);
            if (IsPostBack)
            {
                Server.Transfer("frmVerDocente.aspx");
            }
        }
        #endregion
    }
}