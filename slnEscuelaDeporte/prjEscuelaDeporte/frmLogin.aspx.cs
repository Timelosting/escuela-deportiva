﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;

namespace prjEscuelaDeporte
{
    public partial class frmLogin : System.Web.UI.Page
    {
        #region "Variables Globales"
        private static string strNombreApp;
        private object vrUnico;
        #endregion

        #region Metodos privados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "label label-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "label label-success h5";
            }
        }
        private bool validar()
        {
            if (this.txtUsuario.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar el Usuario", true);
                return false;
            }
            if (this.txtContraseña.Text.Trim() == string.Empty)
            {
                mostrarMsj("Debe ingresar la Contraseña", true);
                return false;
            }
            return true;
        }
        private void mostrarPanel(string panel)
        {
            switch (panel)
            {
                case "pnlDirector":
                    pnlDirector.Visible = true;
                    pnlPrincipal.Visible = true;
                    pnlBtnDirector.Visible = true;
                    break;
                case "pnlEstudiante":
                    pnlEstudiante.Visible = true;
                    pnlPrincipal.Visible = true;
                    pnlBtnEstudiante.Visible = true;
                    break;
                case "pnlProfesor":
                    PnlProfesor.Visible = true;
                    pnlPrincipal.Visible = true;
                    pnlBtnProfesor.Visible = true;
                    break;
                case "pnlPadre":
                    pnlPadre.Visible = true;
                    pnlPrincipal.Visible = true;
                    pnlBtnPadre.Visible = true;
                    break;
                case "inicio":
                    pnlDirector.Visible = false;
                    pnlPrincipal.Visible = false;
                    pnlEstudiante.Visible = false;
                    pnlPadre.Visible = false;
                    PnlProfesor.Visible = false;
                    pnlBtnDirector.Visible = false;
                    pnlBtnEstudiante.Visible = false;
                    pnlBtnPadre.Visible = false;
                    pnlBtnProfesor.Visible = false;
                    break;
            }
        }
        private void Limpiar()
        {
            this.txtUsuario.Text = string.Empty;
            this.txtContraseña.Text = string.Empty;
        }
        private void iniciarSesion(string perfil)
        {
            try
            {
                if (!validar())
                {
                    return;
                }
                switch (perfil)
                {
                    case "director":
                        clsDirectorOP director = new clsDirectorOP(strNombreApp);
                        director.Usuario = this.txtUsuario.Text.Trim();
                        director.Contasena = this.txtContraseña.Text.Trim();
                        if (!director.validarSesionOP())
                        {
                            mostrarMsj(director.Error, true);
                            director = null;
                            return;
                        }
                        vrUnico = director.VrUnico;
                        director = null;
                        if (vrUnico != null)
                        {
                            if (vrUnico.ToString() == this.txtUsuario.Text)
                            {
                                Server.Transfer("frmDirector.aspx");
                            }
                        }
                        break;
                    case "profesor":
                        clsProfesorOP profesor = new clsProfesorOP(strNombreApp);
                        profesor.Usuario = this.txtUsuario.Text.Trim();
                        profesor.Contasena = this.txtContraseña.Text.Trim();
                        if (!profesor.validarSesionOP())
                        {
                            mostrarMsj(profesor.Error, true);
                            profesor = null;
                            return;
                        }
                        vrUnico = profesor.VrUnico;
                        profesor = null;
                        if (vrUnico != null)
                        {
                            if (vrUnico.ToString() == this.txtUsuario.Text)
                            {
                                Server.Transfer("frmPerfilDocente.aspx");
                            }
                        }
                        break;
                    case "estudiante":
                        clsEstudianteOP estudiante = new clsEstudianteOP(strNombreApp);
                        estudiante.Usuario = this.txtUsuario.Text.Trim();
                        estudiante.Contasena = this.txtContraseña.Text.Trim();
                        if (!estudiante.validarSesionOP())
                        {
                            mostrarMsj(estudiante.Error, true);
                            estudiante = null;
                            return;
                        }
                        vrUnico = estudiante.VrUnico;
                        estudiante = null;
                        if (vrUnico != null)
                        {
                            if (vrUnico.ToString() == this.txtUsuario.Text)
                            {
                                Server.Transfer("frmPerfilEstudiante.aspx");
                            }
                        }
                        break;
                    case "padre":
                        clsPadreOP padre = new clsPadreOP(strNombreApp);
                        padre.Usuario = this.txtUsuario.Text.Trim();
                        padre.Contasena = this.txtContraseña.Text.Trim();
                        if (!padre.validarSesionOP())
                        {
                            mostrarMsj(padre.Error, true);
                            padre = null;
                            return;
                        }
                        vrUnico = padre.VrUnico;
                        estudiante = null;
                        if (vrUnico != null)
                        {
                            if (vrUnico.ToString() == this.txtUsuario.Text)
                            {
                                Server.Transfer("frmPerfilPadre.aspx");
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {

                mostrarMsj(ex.Message, true);
            }
                                   
        }
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
            }
            mostrarPanel("inicio");
        }
        protected void btnDirector_Click(object sender, ImageClickEventArgs e)
        {
            mostrarPanel("pnlDirector");
        }
        protected void btnPadre_Click(object sender, ImageClickEventArgs e)
        {
            mostrarPanel("pnlPadre");
        }
        protected void btnEstudiante_Click(object sender, ImageClickEventArgs e)
        {
            mostrarPanel("pnlEstudiante");
        }
        protected void btnDocente_Click(object sender, ImageClickEventArgs e)
        {
            mostrarPanel("pnlProfesor");
        }
        protected void btnIngresarDirector_Click(object sender, EventArgs e)
        {
            iniciarSesion("director");
            Limpiar();
        }
        protected void btnIngresarProfesor_Click(object sender, EventArgs e)
        {
            iniciarSesion("profesor");
            Limpiar();
        }
        protected void btnIngresarEstudiante_Click(object sender, EventArgs e)
        {
            iniciarSesion("estudiante");
            Limpiar();
        }
        protected void btnIngresarPadre_Click(object sender, EventArgs e)
        {
            iniciarSesion("padre");
            Limpiar();
        }
        #endregion
    }
}