﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using libEscuelaDeporteOP;

namespace prjEscuelaDeporte
{
    public partial class frmGrupos : System.Web.UI.Page
    {
        #region Variables globales
        private static string strNombreApp;
        #endregion

        #region Metodos Privados
        private void mostrarMsj(string msj, bool error)
        {
            this.lblMensaje.Text = msj;

            if (msj == string.Empty)
            {
                this.lblMensaje.Visible = false;
                return;
            }
            this.lblMensaje.Visible = true;

            if (error)
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-danger h5";
            }
            else
            {
                this.lblMensaje.CssClass = "alert alert-dismissible fade show alert-success h5";
            }
        }
        private bool validar()
        {
            if (this.ddldeporte.SelectedIndex == 0)
            {
                mostrarMsj("Debe seleccionar el deporte", true);
                return false;
            }
            if (this.ddlhorario.SelectedIndex == 0)
            {
                mostrarMsj("Debe seleccionar el horario", true);
                return false;
            }
            if (this.ddlescenario.SelectedIndex == 0)
            {
                mostrarMsj("Debe seleccionar el escenario", true);
                return false;
            }
            if (this.ddlprofesor.SelectedIndex == 0)
            {
                mostrarMsj("Debe seleccionar el profesor", true);
                return false;
            }
            return true;
        }
        private void mostrarPanel()
        {
            this.pnlMensaje.Visible = true;
        }
        private void Limpiar(string limpiarCampo)
        {
            switch (limpiarCampo)
            {
                case "registrar":
                    this.ddldeporte.SelectedIndex = 0;
                    this.ddlhorario.SelectedIndex = 0;
                    this.ddlprofesor.SelectedIndex = -1;
                    this.ddlescenario.SelectedIndex = -1;
                    this.ddlprofesor.Items.Clear();
                    this.ddlescenario.Items.Clear();
                    break;
                case "limpiarEscenario":
                    this.ddlescenario.SelectedIndex = -1;
                    this.ddlescenario.Items.Clear();
                    break;

            }
            
        }
        private void registrar()
        {
            try
            {
                if (!validar())
                {
                    return;
                }
                clsGrupoOP grupo = new clsGrupoOP(strNombreApp);
                grupo.Deporte = this.ddldeporte.SelectedValue.Trim();
                grupo.IdHorario = int.Parse(this.ddlhorario.SelectedValue.Trim());
                grupo.IdEscenario = int.Parse(this.ddlescenario.SelectedValue.Trim());
                grupo.Documento = this.ddlprofesor.SelectedValue.Trim();

                if (!grupo.registrarOP())
                {
                    mostrarMsj(grupo.Error, true);
                    grupo = null;
                    return;
                }
                mostrarMsj("Registro Exitoso", false);
                mostrarPanel();
                grupo = null;
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        private void llenarDropDown(DropDownList ddlGen)
        {
            try
            {
                clsLlenarComboOP llenar = new clsLlenarComboOP(strNombreApp);
                switch (ddlGen.ID.ToLower())
                {
                    case "ddlescenario":
                        llenar.Deporte = this.ddldeporte.SelectedValue.Trim();
                        break;
                    default:
                        break;
                }
                llenar.DdlGen = ddlGen;
                if (!llenar.llenarDrop())
                {
                    mostrarMsj(llenar.Error, true);
                    llenar = null;
                    return;
                }
            }
            catch (Exception ex)
            {
                mostrarMsj(ex.Message, true);
            }
        }
        #endregion

        #region Eventos
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                strNombreApp = Assembly.GetExecutingAssembly().GetName().Name + ".xml";
                llenarDropDown(ddldeporte);
                llenarDropDown(ddlhorario);
                llenarDropDown(ddlprofesor);
            }
        }
        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            registrar();
            Limpiar("registrar");
        }
        protected void ddldeporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            Limpiar("limpiarEscenario");
            llenarDropDown(ddlescenario);
        }
        #endregion
    }
}