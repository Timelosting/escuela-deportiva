﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmRegistrarDirector.aspx.cs" Inherits="prjEscuelaDeporte.frmRegistrarDirector" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/registroDirector.css" rel="stylesheet" />
    <link href="img/fond.jpg" rel="icon" />
    <title>Registro Director</title>
</head>
<body>
    <form id="frmRegistrarDirector" runat="server">
        <nav class="navbar navbar-expand-md fixed-top navbar-dark bg-dark">
            <a href="frmDirector.aspx" class="navbar-brand">Escuela de deportes</a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle active" data-toggle="dropdown">Directores</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item active" href="frmRegistrarDirector.aspx">Registrar Director</a>
                        <a class="dropdown-item" href="frmVerDirector.aspx">Ver Director</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Padres</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmPadre.aspx">Registrar Padre</a>
                        <a class="dropdown-item" href="frmVerPadre.aspx">Ver Padre</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Docentes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmDocente.aspx">Registrar Docente</a>
                        <a class="dropdown-item" href="frmVerDocente.aspx">Ver Docente</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Estudiantes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmEstudiante.aspx">Ver Estudiantes</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Deportes</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmDeporte.aspx">Crear Deporte</a>
                        <a class="dropdown-item" href="frmVerDeporte.aspx">Ver Deporte</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Escenarios Deportivos</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmEscenario.aspx">Registrar Escenario</a>
                        <a class="dropdown-item" href="frmVerEscenario.aspx">Ver Escenarios</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Grupos</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="frmGrupos.aspx">Crear Grupo</a>
                        <a class="dropdown-item" href="frmActualizarGrupo.aspx">Actualizar Grupo</a>
                        <a class="dropdown-item" href="frmVerGrupo.aspx">Ver Grupos</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Informes</a>
                </li>
                <li class="nav-item" style="margin-left:9em;">
                    <a href="frmLogin.aspx" class="nav-link">Salir</a>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="card">
                <div class="card-header text-center bg-primary">
                    <h5>Registrar Director</h5>
                </div>
                <div class="card-body">
                    <asp:Panel runat="server" ID="pnlMensaje" Visible="false">
                        <div class="alert alert-dismissible fade show" role="alert">
                            <asp:Label ID="lblMensaje" runat="server" CssClass="text-center"></asp:Label>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </asp:Panel>
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" TextMode="Number" ID="txtDocumento" placeholder="Documento de Identidad" required="true"/>
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" placeholder="Nombre" ID="txtNombre" required="true"/>
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" placeholder="Apellido" ID="txtApellido" required="true"/>
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" TextMode="Number" ID="txtEdad" placeholder="Edad" required="true"/>
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" TextMode="Number" ID="txtTelefono" placeholder="Telefono" required="true"/>
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" placeholder="Usuario" ID="txtUsuario" required="true"/>
                    <br />
                    <asp:TextBox runat="server" CssClass="form-control" TextMode="Password" ID="txtPassword" placeholder="Contraseña" required="true" AutoComplete="of"/>
                    <br /> 
                    <asp:Button Text="Registrar" runat="server" ID="btnRegistrar" cssClass="btn btn-block btn-primary" OnClick="btnRegistrar_Click"/>
                </div>
            </div>
        </div>
    </form>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
