﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using libEscuelaDeporte;
using libLlenarGrids;

namespace libEscuelaDeporteOP
{
  public class clsDeporteOP
    {
        #region Constantes
        private const int MENOSUNO = -1;
        private const int CERO = 0;
        private const string REGISTRAR = "registrar";
        private const string BORRAR = "borrar"; 
        #endregion

        #region Atributos
        private string strNombreDeporte;
        private string strNombreApp;
        private string strError;
        private int intIdTipoDeporte;
        #endregion

        #region Constructor
        public clsDeporteOP(string strNombreApp)
        {
            this.strNombreDeporte = string.Empty;
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
            this.intIdTipoDeporte = MENOSUNO;
        }
        #endregion

        #region Propiedades
        public string NombreDeporte { set => strNombreDeporte = value; }
        public string Error { get => strError;  }
        public int IdTipoDeporte { set => intIdTipoDeporte = value; }
        #endregion

        #region Metodos Privados
        private bool Validar(string Validar)
        {
            switch (Validar)
            {
                case REGISTRAR:
                    if (strNombreDeporte == string.Empty)
                    {
                        strError = "Debe ingresar un nombre de deporte";
                        return false;
                    }
                    if (intIdTipoDeporte <= CERO)
                    {
                        strError = "debe seleccionar un tipo de deporte valido";
                        return false;
                    }
                    break;
                case BORRAR:
                    if (strNombreDeporte == string.Empty)
                    {
                        strError = "Debe ingresar un nombre de deporte";
                        return false;
                    }
                    break;
            }
            return true;
        }

        private bool llenarGrid(GridView gvGenerico, DataTable dtDatos)
        {
            try
            {
                clsLlenarGrids llenarGrids = new clsLlenarGrids();
                if (!llenarGrids.llenarGridWeb(gvGenerico, dtDatos))
                {
                    strError = llenarGrids.Error;
                    llenarGrids = null;
                    return false;
                }
                llenarGrids = null;
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public bool registrarOP()
        {
            try
            {
                if (!Validar(REGISTRAR))
                {
                    return false;
                }
                clsDeporteRN deporteRN = new clsDeporteRN(strNombreApp);
                deporteRN.NombreDeporte = strNombreDeporte;
                deporteRN.IdTipoDeporte = intIdTipoDeporte;
                if (!deporteRN.registrar())
                {
                    strError = deporteRN.Error;
                    deporteRN = null;
                    return false;
                }
                deporteRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool borrarOP()
        {
            try
            {
                if (!Validar(BORRAR))
                {
                    return false;
                }
                clsDeporteRN deporteRN = new clsDeporteRN(strNombreApp);
                deporteRN.NombreDeporte = strNombreDeporte;
                if (!deporteRN.borrar())
                {
                    strError = deporteRN.Error;
                    deporteRN = null;
                    return false;
                }
                deporteRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool consultarOP(GridView gvDeporte)
        {
            try
            {
                clsDeporteRN deporte = new clsDeporteRN(strNombreApp);

                if (!deporte.consultar())
                {
                    strError = deporte.Error;
                    deporte = null;
                    return false;
                }
                if (!llenarGrid(gvDeporte, deporte.DsDatos.Tables[0]))
                {
                    deporte = null;
                    return false;
                }
                deporte = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
