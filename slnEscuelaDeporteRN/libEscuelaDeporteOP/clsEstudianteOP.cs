﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using libEscuelaDeporte;
using libLlenarGrids;

namespace libEscuelaDeporteOP
{
    public class clsEstudianteOP
    {
        #region Atributos
        private string strNombreApp;
        private string strError;
        private string strContasena;
        private string strUsuario;
        private object vrUnico;
        #endregion

        #region Constructor
        public clsEstudianteOP(string strNombreApp)
        {
            this.strNombreApp = strNombreApp;
            this.strContasena = string.Empty;
            this.strUsuario = string.Empty;
        }

        #endregion

        #region Propiedades
        public string NombreApp { set => strNombreApp = value; }
        public string Error { get => strError; }
        public string Contasena { set => strContasena = value; }
        public string Usuario { set => strUsuario = value; }
        public object VrUnico { get => vrUnico; }
        #endregion

        #region Metodos Privados
        private bool Validar()
        {
            if (strContasena == string.Empty)
            {
                strError = "Debe ingresar una contraseña";
                return false;
            }
            if (strUsuario == string.Empty)
            {
                strError = "Debe ingresar un usuario";
                return false;
            }
            return true;
        }
        private bool llenarGrid(GridView gvGenerico, DataTable dtDatos)
        {
            try
            {
                clsLlenarGrids llenarGrids = new clsLlenarGrids();
                if (!llenarGrids.llenarGridWeb(gvGenerico, dtDatos))
                {
                    strError = llenarGrids.Error;
                    llenarGrids = null;
                    return false;
                }
                llenarGrids = null;
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public bool consultarOP(GridView gvEstudiante)
        {
            try
            {
                clsEstudianteRN estudiante = new clsEstudianteRN(strNombreApp);

                if (!estudiante.consultar())
                {
                    strError = estudiante.Error;
                    estudiante = null;
                    return false;
                }
                if (!llenarGrid(gvEstudiante, estudiante.DsDatos.Tables[0]))
                {
                    estudiante = null;
                    return false;
                }
                estudiante = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool validarSesionOP()
        {
            try
            {
                if (!Validar())
                {
                    return false;
                }
                clsEstudianteRN estudianteRN = new clsEstudianteRN(strNombreApp);
                estudianteRN.Usuario = strUsuario;
                estudianteRN.Contasena = strContasena;
                if (!estudianteRN.validarSesion())
                {
                    strError = estudianteRN.Error;
                    estudianteRN = null;
                    return false;
                }
                vrUnico = estudianteRN.VrUnico;
                estudianteRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
