﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using libEscuelaDeporte;
using libLlenarGrids;

namespace libEscuelaDeporteOP
{
   public class clsPadreOP
    {
        #region Constantes
        private const int CERO = 0;
        private const string VALIDARSESION = "validarsesion";
        private const string REGISTRAR = "registrar";
        private const string ACTUALIZAR = "actualizar";
        private const string BORRAR = "borrar"; 
        #endregion

        #region Atributos
        private string strNombreApp;
        private string strError;
        private string strDocumento;
        private string strNombre;
        private string strApellido;
        private string strContasena;
        private string strUsuario;
        private int intEdad;
        private int intTelefono;
        private object vrUnico;
        #endregion

        #region Constructor
        public clsPadreOP(string strNombreApp)
        {
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
            this.strDocumento = string.Empty;
            this.strNombre = string.Empty;
            this.strApellido = string.Empty;
            this.strContasena = string.Empty;
            this.strUsuario = string.Empty;
            this.intEdad = CERO;
            this.intTelefono = CERO;
        }
        #endregion

        #region Propiedades

        public string NombreApp { set => strNombreApp = value; }
        public string Error { get => strError; }
        public string Documento { set => strDocumento = value; }
        public string Nombre { set => strNombre = value; }
        public string Apellido { set => strApellido = value; }
        public string Contasena { set => strContasena = value; }
        public string Usuario { set => strUsuario = value; }
        public int Edad { set => intEdad = value; }
        public int Telefono { set => intTelefono = value; }
        public object VrUnico { get => vrUnico; }
        #endregion

        #region Metodos Privados
        private bool Validar(string validarDirector)
        {
            switch (validarDirector)
            {
                case VALIDARSESION:
                    if (strContasena == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (strUsuario == string.Empty)
                    {
                        strError = "Debe ingresar un usuario";
                        return false;
                    }
                    break;
                case REGISTRAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    if (strNombre == string.Empty)
                    {
                        strError = "Debe ingresar el nombre";
                        return false;
                    }
                    if (strApellido == string.Empty)
                    {
                        strError = "Debe ingresar el apellido";
                        return false;
                    }
                    if (strContasena == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (strUsuario == string.Empty)
                    {
                        strError = "Debe ingresar un usuario";
                        return false;
                    }
                    if (intEdad <= CERO)
                    {
                        strError = "La edad no puede ser menor o igual a 0";
                        return false;
                    }
                    break;
                case ACTUALIZAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    if (strNombre == string.Empty)
                    {
                        strError = "Debe ingresar el nombre";
                        return false;
                    }
                    if (strApellido == string.Empty)
                    {
                        strError = "Debe ingresar el apellido";
                        return false;
                    }
                    if (strContasena == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (intEdad <= CERO)
                    {
                        strError = "La edad no puede ser menor o igual a 0";
                        return false;
                    }
                    break;
                case BORRAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    break;
            }
            return true;
        }
        private bool llenarGrid(GridView gvGenerico, DataTable dtDatos)
        {
            try
            {
                clsLlenarGrids llenarGrids = new clsLlenarGrids();
                if (!llenarGrids.llenarGridWeb(gvGenerico, dtDatos))
                {
                    strError = llenarGrids.Error;
                    llenarGrids = null;
                    return false;
                }
                llenarGrids = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public bool registrarOP()
        {
            try
            {
                if (!Validar(REGISTRAR))
                {
                    return false;
                }
                clsPadreRN padreRN = new clsPadreRN(strNombreApp);
                padreRN.Nombre = strNombre;
                padreRN.Apellido = strApellido;
                padreRN.Edad = intEdad;
                padreRN.Documento = strDocumento;
                padreRN.Usuario = strUsuario;
                padreRN.Contasena = strContasena;
                padreRN.Telefono = intTelefono;
                if (!padreRN.registrar())
                {
                    strError = padreRN.Error;
                    padreRN = null;
                    return false;
                }
                padreRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool borrarOP()
        {
            try
            {
                if (!Validar(BORRAR))
                {
                    return false;
                }
                clsPadreRN padreRN = new clsPadreRN(strNombreApp);
                padreRN.Documento = strDocumento;
                if (!padreRN.borrar())
                {
                    strError = padreRN.Error;
                    padreRN = null;
                    return false;
                }
                padreRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool actualizarOP()
        {
            try
            {
                if (!Validar(ACTUALIZAR))
                {
                    return false;
                }
                clsPadreRN padreRN = new clsPadreRN(strNombreApp);
                padreRN.Nombre = strNombre;
                padreRN.Apellido = strApellido;
                padreRN.Edad = intEdad;
                padreRN.Telefono = intTelefono;
                padreRN.Documento = strDocumento;
                padreRN.Contasena = strContasena;
                if (!padreRN.actualizar())
                {
                    strError = padreRN.Error;
                    padreRN = null;
                    return false;
                }
                padreRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool consultarOP(GridView gvPadre)
        {
            try
            {
                clsPadreRN padre = new clsPadreRN(strNombreApp);
                if (!padre.consultar())
                {
                    strError = padre.Error;
                    padre = null;
                    return false;
                }
                if (!llenarGrid(gvPadre, padre.DsDatos.Tables[0]))
                {
                    padre = null;
                    return false;
                }
                padre = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool validarSesionOP()
        {
            try
            {
                if (!Validar(VALIDARSESION))
                {
                    return false;
                }
                clsPadreRN padreRN = new clsPadreRN(strNombreApp);
                padreRN.Usuario = strUsuario;
                padreRN.Contasena = strContasena;
                if (!padreRN.validarSesion())
                {
                    strError = padreRN.Error;
                    padreRN = null;
                    return false;
                }
                vrUnico = padreRN.VrUnico;
                padreRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
