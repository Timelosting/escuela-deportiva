﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using libEscuelaDeporte;
using libLlenarGrids;

namespace libEscuelaDeporteOP
{
   public class clsProfesorOP
    {
        #region Constantes
        private const int CERO = 0;
        private const string BORRAR = "borrar";
        private const string VALIDARSESION = "validarsesion";
        private const string REGISTRAR = "registrar";
        private const string ACTUALIZAR = "actualizar";
        #endregion

        #region Atributos
        private string strNombreApp;
        private string strError;
        private string strDocumento;
        private string strNombre;
        private string strApellido;
        private string strContasena;
        private string strUsuario;
        private int intEdad;
        private int intTelefono;
        private object vrUnico;
        #endregion

        #region Constructor
        public clsProfesorOP(string strNombreApp)
        {
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
            this.strDocumento = string.Empty;
            this.strNombre = string.Empty;
            this.strApellido = string.Empty;
            this.strContasena = string.Empty;
            this.strUsuario = string.Empty;
            this.intEdad = CERO;
            this.intTelefono = CERO;
        }

        #endregion

        #region Propiedades

        public string NombreApp { set => strNombreApp = value; }
        public string Error { get => strError; }
        public string Documento { set => strDocumento = value; }
        public string Nombre { set => strNombre = value; }
        public string Apellido { set => strApellido = value; }
        public string Contasena { set => strContasena = value; }
        public string Usuario { set => strUsuario = value; }
        public int Edad { set => intEdad = value; }
        public int Telefono { set => intTelefono = value; }
        public object VrUnico { get => vrUnico; }
        #endregion

        #region Metodos Privados
        private bool Validar(string validarDirector)
        {
            switch (validarDirector)
            {
                case VALIDARSESION:
                    if (strContasena == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (strUsuario == string.Empty)
                    {
                        strError = "Debe ingresar un usuario";
                        return false;
                    }
                    break;
                case REGISTRAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    if (strNombre == string.Empty)
                    {
                        strError = "Debe ingresar el nombre";
                        return false;
                    }
                    if (strApellido == string.Empty)
                    {
                        strError = "Debe ingresar el apellido";
                        return false;
                    }
                    if (strContasena == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (strUsuario == string.Empty)
                    {
                        strError = "Debe ingresar un usuario";
                        return false;
                    }
                    if (intEdad <= CERO)
                    {
                        strError = "La edad no puede ser menor o igual a 0";
                        return false;
                    }
                    break;
                case ACTUALIZAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    if (strNombre == string.Empty)
                    {
                        strError = "Debe ingresar el nombre";
                        return false;
                    }
                    if (strApellido == string.Empty)
                    {
                        strError = "Debe ingresar el apellido";
                        return false;
                    }
                    if (strContasena == string.Empty)
                    {
                        strError = "Debe ingresar una contraseña";
                        return false;
                    }
                    if (intEdad <= CERO)
                    {
                        strError = "La edad no puede ser menor o igual a 0";
                        return false;
                    }
                    break;
                case BORRAR:
                    if (strDocumento == string.Empty)
                    {
                        strError = "Debe ingresar el documento";
                        return false;
                    }
                    break;
            }
            return true;
        }
        private bool llenarGrid(GridView gvGenerico, DataTable dtDatos)
        {
            try
            {
                clsLlenarGrids llenarGrids = new clsLlenarGrids();
                if (!llenarGrids.llenarGridWeb(gvGenerico, dtDatos))
                {
                    strError = llenarGrids.Error;
                    llenarGrids = null;
                    return false;
                }
                llenarGrids = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public bool registrarOP()
        {
            try
            {
                if (!Validar(REGISTRAR))
                {
                    return false;
                }
                clsProfesorRN profesorRN = new clsProfesorRN(strNombreApp);
                profesorRN.Nombre = strNombre;
                profesorRN.Apellido = strApellido;
                profesorRN.Edad = intEdad;
                profesorRN.Documento = strDocumento;
                profesorRN.Usuario = strUsuario;
                profesorRN.Contasena = strContasena;
                profesorRN.Telefono = intTelefono;
                if (!profesorRN.registrar())
                {
                    strError = profesorRN.Error;
                    profesorRN = null;
                    return false;
                }
                profesorRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool borrarOP()
        {
            try
            {
                if (!Validar(BORRAR))
                {
                    return false;
                }
                clsProfesorRN profesorRN = new clsProfesorRN(strNombreApp);
                profesorRN.Documento = strDocumento;
                if (!profesorRN.borrar())
                {
                    strError = profesorRN.Error;
                    profesorRN = null;
                    return false;
                }
                profesorRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool actualizarOP()
        {
            try
            {
                if (!Validar(ACTUALIZAR))
                {
                    return false;
                }
                clsProfesorRN profesorRN = new clsProfesorRN(strNombreApp);
                profesorRN.Nombre = strNombre;
                profesorRN.Apellido = strApellido;
                profesorRN.Edad = intEdad;
                profesorRN.Telefono = intTelefono;
                profesorRN.Documento = strDocumento;
                profesorRN.Contasena = strContasena;
                if (!profesorRN.actualizar())
                {
                    strError = profesorRN.Error;
                    profesorRN = null;
                    return false;
                }
                profesorRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool consultarOP(GridView gvProfesor)
        {
            try
            {
                clsProfesorRN profesor = new clsProfesorRN(strNombreApp);

                if (!profesor.consultar())
                {
                    strError = profesor.Error;
                    profesor = null;
                    return false;
                }
                if (!llenarGrid(gvProfesor, profesor.DsDatos.Tables[0]))
                {
                    profesor = null;
                    return false;
                }
                profesor = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool validarSesionOP()
        {
            try
            {
                if (!Validar(VALIDARSESION))
                {
                    return false;
                }
                clsProfesorRN profesorRN = new clsProfesorRN(strNombreApp);
                profesorRN.Usuario = strUsuario;
                profesorRN.Contasena = strContasena;
                if (!profesorRN.validarSesion())
                {
                    strError = profesorRN.Error;
                    profesorRN = null;
                    return false;
                }
                vrUnico = profesorRN.VrUnico;
                profesorRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
