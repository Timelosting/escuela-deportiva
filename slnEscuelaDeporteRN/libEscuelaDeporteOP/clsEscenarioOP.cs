﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using libEscuelaDeporte;
using libLlenarGrids;

namespace libEscuelaDeporteOP
{
  public  class clsEscenarioOP
    {
        #region Constantes
        private const int MENOSUNO = -1;
        private const int CERO = 0;
        private const string REGISTRAR = "registrar";
        private const string BORRAR = "borrar"; 
        #endregion

        #region Atributos
        private int intIdEscenario;
        private string strNombreEscenario;
        private string strNombreApp;
        private string strError;
        private int intIdTipoDeporte;
        #endregion

        #region Constructor
        public clsEscenarioOP(string strNombreApp)
        {
            this.strNombreEscenario = string.Empty;
            this.strNombreApp = strNombreApp;
            this.strError = string.Empty;
            this.intIdTipoDeporte = MENOSUNO;
            this.intIdEscenario = MENOSUNO;
        }
        #endregion

        #region Propiedades
        public string NombreEscenario { set => strNombreEscenario = value; }
        public string Error { get => strError; }
        public int IdTipoDeporte { set => intIdTipoDeporte = value; }
        public int idEscenario { set => intIdEscenario = value; }
        #endregion

        #region Metodos Privados
        private bool Validar(string validarEscenario)
        {
            switch (validarEscenario)
            {
                case REGISTRAR:
                    if (strNombreEscenario == string.Empty)
                    {
                        strError = "Debe ingresar un escenario";
                        return false;
                    }
                    if (intIdTipoDeporte <= CERO)
                    {
                        strError = "debe seleccionar un tipo de deporte";
                        return false;
                    }
                    break;
                case BORRAR:
                    if (intIdEscenario <= CERO)
                    {
                        strError = "el id de escenario no puede ser menor o igual que 0";
                        return false;
                    }
                    break;
            }
            return true;
        }
        private bool llenarGrid(GridView gvGenerico, DataTable dtDatos)
        {
            try
            {
                clsLlenarGrids llenarGrids = new clsLlenarGrids();
                if (!llenarGrids.llenarGridWeb(gvGenerico, dtDatos))
                {
                    strError = llenarGrids.Error;
                    llenarGrids = null;
                    return false;
                }
                llenarGrids = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Metodos Publicos
        public bool registrarOP()
        {
            try
            {
                if (!Validar(REGISTRAR))
                {
                    return false;
                }
                clsEscenarioRN escenarioRN = new clsEscenarioRN(strNombreApp);
                escenarioRN.NombreEscenario = strNombreEscenario;
                escenarioRN.IdTipoDeporte = intIdTipoDeporte;
                if (!escenarioRN.registrar())
                {
                    strError = escenarioRN.Error;
                    escenarioRN = null;
                    return false;
                }
                escenarioRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool borrarOP()
        {
            try
            {
                if (!Validar(BORRAR))
                {
                    return false;
                }
                clsEscenarioRN escenarioRN = new clsEscenarioRN(strNombreApp);
                escenarioRN.IdEscenario = intIdEscenario;
                if (!escenarioRN.borrar())
                {
                    strError = escenarioRN.Error;
                    escenarioRN = null;
                    return false;
                }
                escenarioRN = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool consultarOP(GridView gvEscenario)
        {
            try
            {
                clsEscenarioRN escenario = new clsEscenarioRN(strNombreApp);

                if (!escenario.consultar())
                {
                    strError = escenario.Error;
                    escenario = null;
                    return false;
                }
                if (!llenarGrid(gvEscenario, escenario.DsDatos.Tables[0]))
                {
                    escenario = null;
                    return false;
                }
                escenario = null;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
