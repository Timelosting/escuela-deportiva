﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using libConexionBd;

namespace libLlenarGrids
{
    public class clsLlenarGrids
    {
        #region "Atributos"
        private string strNombreApp;
        private string strSQL;
        private string strError;
        private bool blnEjecParametros;
        private SqlParameter[] objParamSQL;
        private clsConexionBd objCnx;
        #endregion

        #region "Constructor"
        public clsLlenarGrids(string nombreAplicacion)
        {
            this.strNombreApp = nombreAplicacion;
            this.strSQL = string.Empty;
            this.strError = string.Empty;
            blnEjecParametros = false;
        }
        public clsLlenarGrids()
        {            
            this.strSQL = string.Empty;
            this.strError = string.Empty;
            blnEjecParametros = false;
        }
        #endregion

        #region "Propiedades"
        public string SQL {set => strSQL = value; }
        public string Error { get => strError; }
        public SqlParameter[] ParametrosSQL { set => objParamSQL = value; }
        #endregion

        #region "Métodos Privados"
        private bool Validar()
        {
            if (string.IsNullOrEmpty(strSQL))
            {
                strError = "Debe enviar el procedimiento almacenado para llenar la grid";
                return false;
            }
            if (string.IsNullOrEmpty(strNombreApp))
            {
                strError = "Debe enviar el nombre de la aplicación para hallar el origen de datos";
                return false;
            }
            return true;
        }
        private bool consultarData()
        {
            try
            {
                if (!Validar())
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);

                objCnx.SQL = strSQL;

                if(objParamSQL != null)
                {
                    blnEjecParametros = true;
                    objCnx.ParametrosSQL = objParamSQL;
                }

                if (!objCnx.llenarDataSet(blnEjecParametros, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                return true;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region "Métodos Públicos"
        public bool llenarGridWindows(DataGridView dgvGenerico)
        {
            try
            {
                if (!consultarData())
                {
                    return false;
                }
                dgvGenerico.DataSource = objCnx.DataSetLleno.Tables[0];
                dgvGenerico.Refresh();
                objCnx.cerrarCnx();
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {   
                objCnx = null;
            }
        }

        public bool llenarGridWeb(GridView gvGenerico)
        {
            try
            {
                if (!consultarData())
                {
                    return false;
                }
                gvGenerico.DataSource = objCnx.DataSetLleno.Tables[0];
                gvGenerico.DataBind();
                objCnx.cerrarCnx();
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objCnx = null;
            }
        }

        public bool llenarGridWeb(GridView gvGenerico, DataTable dtDatos)
        {
            try
            {
                gvGenerico.DataSource = dtDatos;
                gvGenerico.DataBind();                
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objCnx = null;
            }
        }
        #endregion
    }
}
