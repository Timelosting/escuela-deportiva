﻿using libConexionBd;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace libLlenarCombos
{
    public class clsLlenarCombos
    {
        #region "Atributos"
        private string strNombreApp;
        private string strSQL;
        private string strCampoID;
        private string strCampoTexto;
        private string strError;
        private bool blnEjecParametros;
        private SqlParameter[] objParamSQL;
        private clsConexionBd objCnx;
        #endregion

        #region "Constructor"
        public clsLlenarCombos(String nombreAplicacion)
        {
            this.strNombreApp = nombreAplicacion;
            this.strSQL = string.Empty;
            this.strCampoID = string.Empty;
            this.strCampoTexto = string.Empty;
            this.strError = string.Empty;
            blnEjecParametros = false;
        }
        #endregion

        #region "Propiedades"
        public string SQL { set => strSQL = value; }
        public string CampoTexto { set => strCampoTexto = value; }
        public string CampoID {set => strCampoID = value; }
        public string Error { get => strError; }
        public SqlParameter[] ParametrosSQL { set => objParamSQL = value; }
        #endregion

        #region "Métodos Privados"
        private bool Validar()
        {
            if (string.IsNullOrEmpty(strSQL))
            {
                strError = "Debe enviar el procedimiento almacenado para llenar el combo";
                return false;
            }
            if (string.IsNullOrEmpty(strNombreApp))
            {
                strError = "Debe enviar el nombre de la aplicación para hallar el origen de datos";
                return false;
            }
            if (string.IsNullOrEmpty(strCampoID))
            {
                strError = "Debe enviar el nombre del campo Id para llenar el combo";
                return false;
            }
            if (string.IsNullOrEmpty(strCampoTexto))
            {
                strError = "Debe enviar el nombre del campo Texto para llenar el combo";
                return false;
            }
            return true;
        }
        private bool consultarData()
        {
            try
            {
                if (!Validar())
                {
                    return false;
                }
                objCnx = new clsConexionBd(strNombreApp);

                objCnx.SQL = strSQL;

                if (objParamSQL != null)
                {
                    blnEjecParametros = true;
                    objCnx.ParametrosSQL = objParamSQL;
                }

                if (!objCnx.llenarDataSet(blnEjecParametros, true))
                {
                    strError = objCnx.Error;
                    objCnx.cerrarCnx();
                    objCnx = null;
                    return false;
                }
                return true;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
        #region "Métodos Públicos"
        public bool llenarComboWindows(ComboBox cboGenerico)
        {
            try
            {
                if (!consultarData())
                {
                    return false;
                }
                cboGenerico.DataSource = objCnx.DataSetLleno.Tables[0];
                cboGenerico.ValueMember = strCampoID;
                cboGenerico.DisplayMember = strCampoTexto;
                cboGenerico.Items.Add("Seleccione");
                cboGenerico.Refresh();
                objCnx.cerrarCnx();
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objCnx = null;
            }

        }

        public bool llenarComboWeb(DropDownList ddlGenerico)
        {
            try
            {
                if (!consultarData())
                {
                    return false;
                }
                ddlGenerico.DataSource = objCnx.DataSetLleno.Tables[0];
                ddlGenerico.DataValueField = strCampoID;
                ddlGenerico.DataTextField = strCampoTexto;
                ddlGenerico.AppendDataBoundItems = true;
                ddlGenerico.Items.Add("Seleccione");
                ddlGenerico.DataBind();
                objCnx.cerrarCnx();
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objCnx = null;
            }

        }
        #endregion
    }
}
